<?php

namespace App\Services;

use GuzzleHttp\Client as HttpClient;
use App\Customers;

class ShukranClient
{

    /**
     * @var \Illuminate\Config\Repository|mixed|null
     */
    private $clientId, $secret, $clientKey, $baseUrl, $httpClient =null;

    /**
     * ShukranClient constructor.
     * @param null $clientId
     * @param null $secret
     * @param null $baseUrl
     */
    function __construct($clientId=null, $secret=null, $baseUrl=null, $clientKey=null)
    {
        $this->clientId = $clientId ?? config('shukran-configs.client-id');
        $this->secret = $secret ?? config('shukran-configs.client-secret');
        $this->baseUrl = $baseUrl ?? config('shukran-configs.api_base_url');
        $accessKey=generateKey(12);
        storeApiAccessKey($accessKey);
        $this->clientKey = $clientKey ?? encryptKey($accessKey);
        if (is_null($this->httpClient)) {
            $this->setHttpClient($this->getApiClient());
        }
    }

    /**
     * @return HttpClient
     */
    public function getApiClient()
    {
        $client = new HttpClient(['base_uri'=> $this->baseUrl,'headers'=> [
            'client-id' => $this->clientId,
            'client-secret' => $this->secret,
            'client-key' => $this->clientKey,
            'siebel-api-access-key' => config('shukran-configs.siebel_access_key')
        ] ]);

        return $client;

    }

    /**
     * @param HttpClient|null $httpClient
     *
     * @return $this
     */
    public function setHttpClient($httpClient)
    {
        $this->httpClient = $httpClient;

        return $this;
    }

    /**
     * @param $cardNo
     * @return string
     */
    public function updateSiebelCard($cardNo){
        $request = $this->httpClient->get( 'v1/siebel/card/'.$cardNo.'/update');
        $res = $request->getBody()->getContents();
        forgetApiAccessKey();
        return ['message' => $res];
    }

    public function  updateInvoice($invoiceNumber){
        $request = $this->httpClient->get( 'v1.2/siebel/invoice/'.$invoiceNumber.'/update');
        $res = $request->getBody()->getContents();
        forgetApiAccessKey();
        return ['message'=>$res];
    }

    public function validateSiebelCard($cardNo, $customerId){
        $request = $this->httpClient->get( 'v1/siebel/validate/card/'.$cardNo);
        $result = $request->getBody()->getContents();
        $res =json_decode($result);
        if($res->status == config('common.seibel_card_doesnot_exists')){
            return ['success' => $this->updateCustomerValues($customerId)];
        }
        if($res->status == config('common.seibel_card_exists')){
            return ['success' => 0, 'status' => 422, 'error1' => true, 'message'=>'Seibel Card Exists'];
        }
        if($res->status == config('common.seibel_not_connected')){
            return ['success' => 0, 'status' => 422, 'error2' => true, 'message'=>'Unable to connect Seibel'];
        }
        forgetApiAccessKey();
    }


    public function updateCustomerValues($customerId){
        return Customer::where('id',$customerId)->update(['email' => null, 'phone' => null, 'active' => 0]);

    }

    /**
     * @param $method
     * @param $url
     * @param $requestBody
     * @param $headers
     * @return mixed
     */
    public function sendApiRequest($method, $url, $requestBody, $headers){
        if($method == "get"){
            return $this->getApiResponse($url, $headers);
        }
        else{
            return $this->postApiResponse($url, $requestBody, $headers);
        }

    }

    /**
     * @param $url
     * @param $headers
     * @return mixed
     */
    public function getApiResponse($url, $headers){
        $request = $this->httpClient->get($url, ['headers'=> is_null($headers) ? [] :json_decode($headers, true)]);
        $res = $request->getBody()->getContents();
        forgetApiAccessKey();
        return $res;
    }

    /**
     * @param $url
     * @param $requestBody
     * @param $headers
     * @return mixed
     */
    public function postApiResponse($url, $requestBody, $headers)
    {
        $headers = array_merge(json_decode($headers, true), ['Content-Type' => 'application/json']);
        $request = $this->httpClient->post($url,['json'=> \GuzzleHttp\json_decode($requestBody),'headers'=> $headers]);
        $res = $request->getBody()->getContents();
        forgetApiAccessKey();
        return $res;
    }

}