<?php

namespace App\Http\Controllers;

use App\Events\ShippingStatusUpdated;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Pusher\Pusher;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function pushMessage(){
        $data['update'] = 'order updated';
        event(new ShippingStatusUpdated($data));
//        $options = array(
//            'cluster' => 'ap2',
//            'useTLS' => true
//        );
//        $pusher = new Pusher(
//            '70168b26d10a1ccdf979',
//            '1892f108a524ed0d118d',
//            '730362',
//            $options
//        );
//        $data['message'] = 'hello world';
//        $pusher->trigger('order', 'ShippingStatusUpdated', $data);

        
    }

    public function getCustomers(){

    }
}
