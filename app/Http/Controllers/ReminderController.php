<?php

namespace App\Http\Controllers;

use App\Repositories\ReminderRepository;
use Illuminate\Http\Request;

class ReminderController extends Controller
{

    private $reminderRepository;


    public function __construct(ReminderRepository $reminderRepository)
    {
        $this->reminderRepository = $reminderRepository;
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createTask(Request $request)
    {
        $data = $request->all();
        return $this->reminderRepository->createTaskForUser($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getListOfTasks($id)
    {
        return $this->reminderRepository->getTasksListForUser($id);
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        return $this->reminderRepository->updateTasks($id, $data);
    }

  
}
