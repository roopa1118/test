<?php

namespace App\Repositories;



use App\Models\Tasks;

class ReminderRepository{
    
    public function model()
    {
        return Tasks::class;
    }

    public function getTasksListForUser($id)
    {
        return Tasks::select('title', 'notes', 'status')->where('user_id', $id)->get()->toArray();
    }

    public function createTaskForUser($data)
    {
        $tasks = new Tasks();

        $tasks->title = $data['title'];
        $tasks->notes = $data['notes'];
        $tasks->duedate = $data['duedate'];
        $tasks->user_id = $data['user_id'];
        $tasks->status = "created";

        return $tasks->save();
    }

    public function updateTasks($id, $data)
    {
        $data['status'] ='updated';
        return Tasks::where('user_id', $id)->update([$data]);
    }
}